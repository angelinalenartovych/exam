#include <iostream>
#include <cassert>
#include <string>
#include <cstdlib>

std::string printSum(int sum) {
    return "Sum: " + std::to_string(sum);
}

std::string printAverage(int sum, int cnt) {
    return "Average: " + std::to_string((sum + 0.0) / cnt);
}

void TestPrintSum() {
    assert(printSum(1).compare("Sum: 1") == 0);
    assert(printSum(2).compare("Sum: 2") == 0);
    assert(printSum(3).compare("Sum: 3") == 0);
    assert(printSum(4).compare("Sum: 4") == 0);

    std::cout << "TestPrintSum PASSED" << std::endl;
}

void TestPrintAverage() {
    assert(printAverage(2, 2).compare("Average: 1.000000") == 0);
    assert(printAverage(4, 2).compare("Average: 2.000000") == 0);
    assert(printAverage(6, 2).compare("Average: 3.000000") == 0);
    assert(printAverage(8, 2).compare("Average: 4.000000") == 0);

    std::cout << "TestPrintAverage PASSED" << std::endl;
}

int main() {
    int sum = 0;
    int cnt = 0;
    int n;

    do {
        std::cin >> n;

//        try {
//            if (error condition) {
//                throw -1;
//            }
//        } catch (int error) {
//            std::cout << "Error: Number is invalid and try again later" << std::endl;
//            return EXIT_FAILURE;
//        }

        if (n > 5) {
            sum += n;
            cnt++;
        }
    } while(n >= -5);

    if(cnt == 0) {
        std::cout << "No numbers greater than 5" << std::endl;
        return 0;
    }

    std::cout << printSum(sum) << std::endl;
    std::cout << printAverage(sum, cnt) << std::endl;

    TestPrintSum();
    TestPrintAverage();

    return EXIT_SUCCESS;
}

